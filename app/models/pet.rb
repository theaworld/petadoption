class Pet < ApplicationRecord
  belongs_to :user
  belongs_to :category

  has_many :costs
  has_many_attached :pet_photos
  accepts_nested_attributes_for :costs
  has_rich_text :pet_description

  geocoded_by :address
  after_validation :geocode, if: :address_changed? 

  
  # Validations Start Here
  attr_accessor :validates_step

  with_options if: :validates_step_1? do |x|
    x.validates :weight, numericality: { 
      only_integer: true,
      greater_than_or_equal_to: 0,
      less_than_or_equal_to: 150,
      message: " Can not be more than 150 KG and no less than 0 KG"
    }, on: :update
    x.validates :age, numericality: { 
      only_integer: true,
      greater_than_or_equal_to: 0,
      less_than_or_equal_to: 40,
      message: " Can not be older than 40 years old and no younger than 0 years"
    }, on: :update

    x.validates :pet_description, length: { 
      minimum: 150, 
      maximum: 3000,
      message: " Please put a meaningful descriptipn of the pet, at least 150 characters and no more than 3000"
    }
    x.validates :michrochip, format: {
      with: /^\w+$/i,
      multiline: true,
      message: " can only contain letters and numbers"},
      length: {
        minimum: 10,
        maximum: 100
      }, allow_blank: true, on: :update
  end

  with_options if: :validates_step_3? do |x|
    x.validates :pet_photos, presence: true
  end

  private

    def validates_step_1?
      self.validates_step == 1
    end

    def validates_step_2?
      self.validates_step == 2
    end

    def validates_step_3?
      self.validates_step == 3
    end

end
