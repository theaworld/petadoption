class AddLatAndLongAndAddressToPet < ActiveRecord::Migration[6.0]
  def change
    add_column :pets, :latitude, :float
    add_column :pets, :longitude, :float
    add_column :pets, :address, :string
  end
end
